const Server = require("../models/ServerResponse");
const async = require('async');

const index = (req, res) => {
	Server.distinct("players.name").exec((err, docs) => { 
		res.render("players/page", { title: "Players", docs})
	})

};

/**
 * Player Details Page
 * 
 * @param {*} req - An express request
 * @param {*} res - An express response
 */
const player = (req, res) => {
	res.render("players/page", { title: "Top Players" });
};

/**
 * Export all page endpoints
 */
module.exports = {
	index,
	player,
};
