/**
 * Imports
 */
const express = require('express');
const mongoose = require('mongoose');
const argv = require('yargs').argv;
const path = require('path');

/**
 * Configuration
 */
const config = {
	mongo: argv.mongo || "mongodb://localhost/acrisius",
	port: argv.port || 5889
};

/**
 * Instansiate Server
 */
const app = express();

/**
 * Configure Express Server
 */
app.set("port", config.port);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })); // Static Content
app.use(express.static(path.join(__dirname, "bower_components"), { maxAge: 31557600000 })); // Bower Content


/**
 * MongoDB Connection and Configuration
 */
mongoose.Promise = require("bluebird");
mongoose.connect(config.mongo, { useMongoClient: true }, () => {
	console.log("Connected to MongoDB!");
});
/**
 * Import Controllers
 */
const indexController = require('./controllers/index');
const playerController = require('./controllers/players');
const serverController = require('./controllers/servers');

/**
 * Connection Passing
 */
app.use((req, res, next) => {
	req.mongoose = mongoose;
	return next();
})

/**
 * Default Route
 */
app
	.get('/', indexController.index);

/**
 * Player Routes
 */
app
	.get('/players', playerController.index)
	.get('/players/:playerName', playerController.player);

/**
 * Server Routes
 */
app
	.get('/servers', serverController.index)
	.get('/servers/:serverName', serverController.server);

/**
 * Start Server
 */
app.listen(app.get("port"), () => {
	console.log(`Acrisius Front-End available on port ${config.port}`)
})