const mongoose = require('mongoose');
const ObjectId = mongoose.SchemaType.ObjectId;

const serverSchema = new mongoose.Schema({
	masterId: Object,
	name: String,
	host: String,
	port: Number,
	hostPlayer: String,
	isDedicated: Boolean,
	sprintEnabled: Boolean,
	sprintUnlimitedEnabled: Boolean,
	assassinationEnabled: Boolean,
	VoIP: Boolean,
	teams: Boolean,
	redScore: Number,
	blueScore: Number,
	map: String,
	mapFile: String,
	variant: String,
	variantType: String,
	status: String,
	numPlayers: Number,
	maxPlayers: Number,
	xnkid: String,
	xnaddr: String,
	gameVersion: String,
	eldewritoVersion: String,
	mods: Array,
	players: [{
		name: String,
		score: Number,
		kills: Number,
		assists: Number,
		deaths: Number,
		betrayals: Number,
		timeSpentAlive: Number,
		suicides: Number,
		bestStreak: Number,
		team: Number,
		isAlive: Boolean,
		uid: String
	}]
});

module.exports = mongoose.model("GameServer", serverSchema);